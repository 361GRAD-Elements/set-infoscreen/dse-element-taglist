<?php

/**
 * 361GRAD Taglist Element
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementTaglist\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseTaglist
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseTaglist extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_taglist';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->title = $this->headline;

            $arrTableRow = StringUtil::deserialize($this->dse_teaser_tags);

            $values = '';
            foreach ($arrTableRow as $row) {
                $values .= implode(" | ", $row) . "<br>";
            }

            $objTemplate->wildcard = $values;

            return $objTemplate->parse();
        }
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {

        // Extract serialized data from table row
        $arrTableRow = StringUtil::deserialize($this->dse_teaser_tags);

        $this->Template->tableRow = $arrTableRow;

        // Set up Images in template
        $self = $this;

        $this->Template->getImageObject = function () use ($self) {
            return call_user_func_array(array($self, 'getImageObject'), func_get_args());
        };

        return true;
    }


    /**
     * Get an image object from uuid
     *
     * @param       $uuid
     * @param null  $size
     * @param null  $maxSize
     * @param null  $lightboxId
     * @param array $item
     *
     * @return \FrontendTemplate|object
     */
    public function getImageObject($uuid, $size = null, $maxSize = null, $lightboxId = null, $item = array())
    {
        global $objPage;

        if (!$uuid) {
            return null;
        }

        $image = FilesModel::findByUuid($uuid);

        if (!$image) {
            return null;
        }

        try {
            $file = new File($image->path, true);
            if (!$file->exists()) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

        $imageMeta = $this->getMetaData($image->meta, $objPage->language);

        if (is_string($size) && trim($size)) {
            $size = deserialize($size);
        }
        if (!is_array($size)) {
            $size = array();
        }
        $size[0] = isset($size[0]) ? $size[0] : 0;
        $size[1] = isset($size[1]) ? $size[1] : 0;
        $size[2] = isset($size[2]) ? $size[2] : 'crop';

        $image = array(
            'id'        => $image->id,
            'uuid'      => isset($image->uuid) ? $image->uuid : null,
            'name'      => $file->basename,
            'singleSRC' => $image->path,
            'size'      => serialize($size),
            'alt'       => $imageMeta['title'],
            'imageUrl'  => $imageMeta['link'],
            'caption'   => $imageMeta['caption'],
        );

        $image = array_merge($image, $item);

        $imageObject = new FrontendTemplate('dse_image_object');
        $this->addImageToTemplate($imageObject, $image, $maxSize, $lightboxId);
        $imageObject = (object) $imageObject->getData();

        if (empty($imageObject->src)) {
            $imageObject->src = $imageObject->singleSRC;
        }

        return $imageObject;
    }

    /**
     * Creates special label for checkboxes with a colored style
     *
     * @param string $bgc   The Background Color.
     * @param string $fgc   The Foreground Color.
     * @param string $title The Label Title.
     *
     * @return string
     */
    public static function refColor($bgc, $fgc, $title)
    {
        $style = 'display:inline-block;width:100px;text-align:center;';
        return '<span style="background-color:' . $bgc . ';color:' . $fgc . ';' . $style . '">' . $title . '</span>';
    }
}
